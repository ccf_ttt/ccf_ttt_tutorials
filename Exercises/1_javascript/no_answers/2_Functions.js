/**
 * Objects
 * 1. Creating JavaScript Objects
 * 2. Reading JavaScript Objects
 */

var anObject = {};

/**
 * Exercise 1: Create a function named sayHi that does not take in any parameters
 * The sayHi function should print to console the String 'Hi!'.
 */
console.log('Exercise 1 Start')
// Write your code for exercise 1 here


/**
 * Exercise 2: Assign a property named aProperty to anObject
 * Assign the sayHi function to the value of the new property
 */
console.log('Exercise 2 Start')
// Write your code for exercise 2 here


/**
 * Exercise 3: Invoke the function sayHi from anObject
 */
console.log('Exercise 3 Start')
// Write your code for exercise 3 here


/**
 * Exercise 4: Create another function named sayBye, that takes in a parameter called aFunction
 * The sayBye should first invoke aFunction and then print to console the String 'Bye!'.
 */
console.log('Exercise 4 Start')
// Write your code for exercise 4 here


/**
 * Exercise 5: Invoke the function sayBye by passing in the function sayHi into parameter aFunction
 */
console.log('Exercise 5 Start')
// Write your code for exercise 5 here


/**
 * Exercise 6: Invoke the function sayBye by passing in an anonymous function into the aFunction parameter.
 * The anonymous function should print to console the String 'HiHi!'
 */
console.log('Exercise 6 Start')
// Write your code for exercise 6 here


/** 
 * See the button object below.
 * button object simulates a button on a client
 * Assume that whenever the button is pressed (function click is called), the button's onClick function is invoked.
 * Important Note: You should not change any code inside button
 */
const button = {
    onClick: function () { },
    click: function () {
        this.onClick()
    }
}

/**
 * Exercise 7.1: 
 * See function printToConsole
 * When button is pressed (button.click), it should invoke printToConsole and print 'Button Pressed!'
 */
function printMessageToConsole() {
    console.log('Button Pressed!')
}

function exercise7_1() {
    console.log('Exercise 7.1 Start')
    // Write your code for exercise 7.1 here

    button.click() // Do not remove this
}
exercise7_1()

/**
 * Exercise 7.2: 
 * See function printToConsole
 * When button is pressed (button.click), it should invoke printToConsole and print 'Custom Message'
 */
function printCustomMessageToConsole(message) {
    console.log(message)
}

function exercise7_2() {
    console.log('Exercise 7.2 Start')
    const message = 'custom message'
    // Write your code for exercise 7.2 here

    button.click() // Do not remove this
}
exercise7_2()