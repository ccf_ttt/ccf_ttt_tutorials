// Show synchronous demo

/**
 * See Sleep function below
 * Note: This code is just a demo and is considered very bad practice
*/
function sleep(milliseconds) {
    var endtime = new Date().getTime() + milliseconds;
    while (new Date().getTime() < endtime);
}

function slowDatabaseRead() {
    sleep(2000)
    console.log("I'm so slow");
    return {
        a: 1,
        b: 2
    }
}

/*
* Exercise 1: Use Promise to call the API
*/
function exercise_1() {
    console.log("Exercise 1 Start");
    
    // Write your code for exercise 4 here

    console.log("Exercise 1 End");
}

exercise_1();