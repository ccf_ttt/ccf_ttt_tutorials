/**
 * Exercise 1: print hello world to the console
 */
console.log('Exercise 1 Start')
// Write your code for exercise here
console.log('hello world!')

/**
 * Exericse 2: import math.js and use the sum function to calculate what is 1 + 2.
 * Print the answer to the console
 */
var math = require("./math")
console.log(math.sum(1, 2));
