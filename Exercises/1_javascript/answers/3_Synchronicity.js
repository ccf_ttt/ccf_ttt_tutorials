// Show synchronous demo

/**
 * See Sleep function below
 * Note: This code is just a demo and is considered very bad practice
*/
function sleep(milliseconds) {
    var endtime = new Date().getTime() + milliseconds;
    while (new Date().getTime() < endtime);
}

function slowDatabaseRead() {
    sleep(2000)
    console.log("I'm so slow");
    return {
        a: 1,
        b: 2
    }
}

/**
 * Exercise 1: Demo a synchronous blocking code
 */
function exercise_1() {
    console.log("Exercise 1 Start");
    slowDatabaseRead();
    console.log("Exercise 1 End");
}

/**
 * Exercise 2: Demo a asynchronous non-blocking code
 * Notice the order in which 'I'm so slow' is printed.
 */
function exercise_2() {
    console.log("Exercise 2 Start")
    setTimeout(function () {
        slowDatabaseRead()
    }, 0)
    console.log("Exercise 2 End");
}

/**
 * Exercise 3: Demo how to handle result of synchronous slow function
 */
function exercise_3() {
    console.log("Exercise 3 Start");
    const data = slowDatabaseRead();
    console.log(data.a + data.b)
    console.log("Exercise 3 End");
}

/**
 * Exercise 4: [BAD] Demo how to handle result of asynchronous slow function
 */
function exercise_4() {
    console.log("Exercise 4 Start");
    var data = {a: 0, b: 0};

    setTimeout(function () {
        data = slowDatabaseRead();
    }, 0)

    console.log(data.a + data.b);
    console.log("Exercise 4 End");
}

/**
 * Exercise 5: [GOOD] Demo how to handle reuslt of asynchronous slow function
 */
function exercise_5() {
    console.log("Exercise 5 Start");
    setTimeout(function() {
        var data = slowDatabaseRead();
        console.log(data.a + data.b);
    })
    console.log("Exercise 5 End");
}

exercise_1();
exercise_2();
exercise_3();
exercise_4();
exercise_5();