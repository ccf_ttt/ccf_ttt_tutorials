/**
 * Objects
 * 1. Creating JavaScript Objects
 * 2. Reading JavaScript Objects
 */

var anObject;

/**
 * Exercise 1: assing anObject with 3 properties and values
*     a: 'apples', b: 'oranges', c: 'pineapples'
 */
console.log('Exercise 1 Start')
// Write your code for exercise 1 here
anObject = {
    a: 'apples',
    b: 'oranges',
    c: 'pineapples'
};
console.log(anObject)

/**
* Exercise 2: Add another property, d
* d is another object with 1 property and value
*    quantity: 4
*/
console.log('Exercise 2 Start')
// Write your code for exercise 2 here
anObject.d = {
    quantity: 4
};

/**
* Exercise 3: Remove the property b
*/
console.log('Exercise 3 Start')
// Write your code for exercise 3 here
delete anObject['b']
console.log(anObject)

/**
* Exercise 4: Add another property e,
* e is a list of 4 elements
* Element 1 is a String 'bicycle'
* Element 2 is a Number 10
* Element 3 is a boolean false
* Element 4 is a Number 2
*/
console.log('Exercise 4 Start')
// Write your code for exercise 4 here
anObject.e = ['bicycle', 10, false, 2];

/**
 * Exercise 5: Create a function called sumNumbers, 
 * that takes in a list as a parameter and sums all numbers in the list
*/
console.log('Exercise 5 Start')
// Write your code for exercise 5 here
function sumNumbers(numbers) {
    var sum = 0;
    numbers.forEach(element => {
        if (!isNaN(element)) {
            sum += element
        }
    });
    return sum;
}

/**
 * Exercise 6: Using the function in exercise 5, find and print the sum of all the numbers in e and d
 * Hint: Answer should be 12
*/
console.log('Exercise 6 Start')
// Write your code for exercise 6 here
console.log(sumNumbers(anObject.e));