function sayHello() {
    console.log('Hello World!');
}

function generateTable(data) {
    data.forEach(person => {
        var rowNode = document.createElement("tr");

        var firstNameNode = document.createElement("td");
        firstNameNode.appendChild(document.createTextNode(person.first_name));

        var lastNameNode = document.createElement("td");
        lastNameNode.appendChild(document.createTextNode(person.last_name));

        rowNode.appendChild(firstNameNode);
        rowNode.appendChild(lastNameNode);

        document.getElementById("people-table").appendChild(rowNode)
    });
}