function showAnAlertBox() {
    alert("This is an alert!")
}

function printToTheConsole() {
    console.log("This is the console!")
}

function reqListener() {
    console.log(this.responseText);
}

/*
 What other events are there? 
 [https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest]
 */
function sendXmlHttpRequest() {
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", reqListener);
    oReq.open("GET", "http://httpbin.org/anything");
    oReq.send();
}

function getSomeFX() {
    var exchangRatesUrl = "https://api.exchangeratesapi.io/latest?base=USD&symbols=SGD"

    fetch(exchangRatesUrl)
        .then(function (response) {            
            response.json().then(function (data) {                
                console.log(data.rates.SGD)
            })
        });
}


function getFXFromInput() {
    const baseCurrency = document.getElementById("base-currency-input")
    const targetCurrency = document.getElementById("target-currency-input")
    getFX(baseCurrency.value, targetCurrency.value)
}

function getFX(baseCurrency, targetCurrency) {
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function () {
        const responseJson = JSON.parse(this.response);
        document.getElementById("result-exchange-rate").textContent = responseJson.rates[targetCurrency]
    });
    oReq.open("GET", `https://api.exchangeratesapi.io/latest?base=${baseCurrency}&symbols=${targetCurrency}`);
    oReq.send();
}